﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TataPower.BillingConfident.Helpers
{
    public class BillingConfidents
    {
        private SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public int GetConfigValue()
        {
            try
            {
                int configValue = 0;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "SELECT CONFIG_VALUE FROM [dbo].[CONFIG_READ_REQUEST] WHERE CONFIG_TYPE='LookBackDaysforConfidentMeters'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        configValue = reader["CONFIG_VALUE"] == DBNull.Value ? 0 : Convert.ToInt32(reader["CONFIG_VALUE"]);
                    }
                    connection.Close();
                    return configValue;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetBillingConfidentReport(string endTime, int daysFrLkbk)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = @"
                    Declare @cnt int
                    Declare @latestValidFrom datetime

                    Create table #reg(
                    ID int,
                    Meter_ID varchar(20),
                    Utility_ID varchar(20),
                    END_TIME datetime)

                    Create table #reg2(
                    ID int,
                    Meter_ID varchar(20),
                    Utility_ID varchar(20),
                    END_TIME datetime)

                    truncate table [dbo].[Billing_Confident]


                    insert into #reg
                    Select AA.ID,AA.Meter_ID,AA.Utility_ID,AA.END_TIME from (
                    Select ROW_NUMBER() OVER(PARTITION BY Eq.ID,Eq.Meter_ID, Eq.Utility_ID ORDER BY RD.[End_Time] DESC) AS 'RowNumber'
                    ,Eq.ID,Eq.Meter_ID, Eq.Utility_ID,RD.[End_Time] from Equipment Eq 
                    JOIN REGISTER_DATA RD ON Eq.ID = RD.EQUIPMENT_ID
                    WHERE (end_time >= getdate() - " + daysFrLkbk + @") and Param_id in (Select ID from PARAMETER_MASTER Where param_type = 'Register' and isprofile='P')) AA --Lookback days
                    Where RowNumber=1

                    insert into Billing_Confident
                    Select ID,Meter_ID,Utility_ID,NULL,'Daily Read not available' As ReasonForNonConfident,GetDate(),'Schedule' from Equipment where Meter_ID Not in (Select Meter_ID from #reg)


                    Select @cnt=count(1) from #reg
                    if(@cnt > 0)
	                    BEGIN
		
		                    Insert into #reg2
		                    Select ID,Meter_ID,Utility_ID,END_TIME from
		                    (Select ROW_NUMBER() OVER(PARTITION BY Equip.Meter_ID, Equip.Utility_ID ORDER BY RD.[End_Time] DESC) AS 'RowNum',
		                    Equip.ID,Equip.Meter_ID, Equip.Utility_ID, RD.END_TIME,' ' As ReasonForNonConfident
		                    FROM REGISTER_DATA RD
		                    JOIN Equipment Equip ON RD.EQUIPMENT_ID = Equip.ID 
		                    JOIN #reg R ON R.ID = RD.EQUIPMENT_ID
		                    where PARAM_ID in (select ID from PARAMETER_MASTER where PARAM_NAME='MDKW') --AND 
		                    ) A

		                    insert into Billing_Confident
		                    Select distinct ID,Meter_ID,Utility_ID,max(END_TIME) As END_TIME, 'Reset Demand Not Available' AS ReasonForNonConfident,GetDate(),'Schedule' from #reg2
		                    Where ID Not in (Select ID from #reg2 where END_TIME='" + endTime + @"')
		                    group by ID,Meter_ID,Utility_ID

		                    Delete from #reg2 where ID in(
		                    Select distinct ID from #reg2
		                    Where ID Not in (Select ID from #reg2 where END_TIME='" + endTime + @"'))

		                    Insert into Billing_Confident
		                    Select ID,Meter_ID,Utility_ID,END_TIME,'Demand Peak time not valid' As ReasonForNonConfident,GetDate(),'Schedule'
		                    FROM #reg2 where ID NOT IN (
			                    Select distinct R.ID
			                    FROM REGISTER_DATA RD
			                    JOIN Equipment Equip ON RD.EQUIPMENT_ID = Equip.ID 
			                    join DevLoc_Device_Link DLL ON RD.EQUIPMENT_ID = DLL.EquipmentId AND RD.DEMAND_PEAK_TIME < DLL.ValidFromDate
			                    JOIN #reg R ON R.ID = DLL.EquipmentId
			                    )

	                    END
                    Select * from Billing_Confident 

                    drop table #reg
                    drop table #reg2
                    ";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
