﻿using System;
using TataPower.BillingConfident.Helpers;

namespace TataPower.BillingConfident
{
    class Program
    {
        static void Main(string[] args)
        {
            BillingConfidents billingConfident = new BillingConfidents();
                        
            int configValue = billingConfident.GetConfigValue();

            DateTime date = DateTime.Now;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);

            billingConfident.GetBillingConfidentReport(firstDayOfMonth.ToString("yyyy-MM-dd"), configValue);

        }
    }
}
